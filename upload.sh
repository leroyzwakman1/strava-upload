#!/bin/bash 
source .token

while read file ; do
	curl -X POST https://www.strava.com/api/v3/uploads \
	    -H "Authorization: Bearer ${OAUTH_TOKEN}"  \
	    -F file=@"$file" -F data_type="tcx"
done < <(find "/home/leroy/TomTom" -name "*.tcx" -mtime -1)

#fetch a OAUTH_TOKEN? 
#curl -X POST https://www.strava.com/oauth/token \
#   -F client_id=${CLIENT_ID} \
#   -F client_secret=${CLIENT_SECRET}\
#   -F code=${CODE} | tee json2
#
